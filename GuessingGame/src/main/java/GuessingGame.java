import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



public class GuessingGame extends JFrame {

    public static final String RESOURCES = "src/main/resources/";

    private static final Font FONT_COMIK = new Font("Comic Sans MS", Font.BOLD, 15);
    ;
    private Icon iconWinner = new ImageIcon("4299567.png");
    private final Icon iconItem = new ImageIcon("coo.gif");
    private final Icon iconExit = new ImageIcon("exit.png");
    private Icon iconAbout = new ImageIcon("80840.png");
    private final Map<String, Integer> properties = new HashMap<>();

    private final JLabel lblGuess;

    private final JLabel lblOutput;

    private final JTextField txtGuess;

    JRadioButtonMenuItem itemRangeOne;

    JRadioButtonMenuItem itemRangeTwo;

    JRadioButtonMenuItem itemRangeThree;
    private int numberOfTries;
    private int theNumber;
    private String message = "";




    public GuessingGame() {
        setTitle("Igor`s guessing game");
        setResizable(false);
        setSize(new Dimension(450, 300));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                writeProperties();
            }
        });
        getContentPane().setLayout(null);

        JMenu menuNewGame = new JMenu("New Game");
        JMenuItem itemNewGame = new JMenuItem("new game...", iconItem);
        itemNewGame.addActionListener(e -> newGame());
        menuNewGame.add(itemNewGame);
        menuNewGame.addSeparator();
        JMenuItem itemExit = new JMenuItem("exit", iconExit);
        itemExit.addActionListener(e -> System.exit(0));
        menuNewGame.add(itemExit);


        JMenu menuStats = new JMenu("Stats");
        JMenuItem itemStats = new JMenuItem("stats...");
        itemStats.addActionListener(e -> stats());
        menuStats.add(itemStats);

        JMenu menuSettings = new JMenu("Settings");

        itemRangeOne = new JRadioButtonMenuItem("1 to 10");
        itemRangeOne.addActionListener(e -> {
            properties.replace("range", 10);
            newGame();
        });

        itemRangeTwo = new JRadioButtonMenuItem("1 to 100");
        itemRangeTwo.addActionListener(e -> {
            properties.replace("range", 100);
            newGame();
        });

        itemRangeThree = new JRadioButtonMenuItem("1 to 1000");
        itemRangeThree.addActionListener(e -> {
            properties.replace("range", 1000);
            newGame();
        });

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(itemRangeOne);
        buttonGroup.add(itemRangeTwo);
        buttonGroup.add(itemRangeThree);

        menuSettings.add(itemRangeOne);
        menuSettings.add(itemRangeTwo);
        menuSettings.add(itemRangeThree);

        JMenu menuAbout = new JMenu("About");
        JMenuItem itemAbout = new JMenuItem("about...");
        itemAbout.addActionListener(e -> aboutGame());
        menuAbout.add(itemAbout);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuNewGame);
        menuBar.add(menuStats);
        menuBar.add(menuSettings);
        menuBar.add(menuAbout);

        setJMenuBar(menuBar);

        JLabel lblTitle = new JLabel("Hi-Lo Guessing Game");
        lblTitle.setBounds(10, 37, 414, 24);
        lblTitle.setFont(FONT_COMIK);
        lblTitle.setHorizontalAlignment(JTextField.CENTER);
        getContentPane().add(lblTitle);

        lblGuess = new JLabel();
        lblGuess.setBounds(10, 98, 292, 14);
        lblGuess.setFont(FONT_COMIK);
        lblGuess.setHorizontalAlignment(JLabel.LEFT);
        getContentPane().add(lblGuess);

        txtGuess = new JTextField();
        txtGuess.setBounds(302, 95, 43, 20);
        txtGuess.setFont(FONT_COMIK);
        txtGuess.setHorizontalAlignment(JTextField.RIGHT);
        txtGuess.addActionListener(e -> checkGuess());
        getContentPane().add(txtGuess);

        JButton btnGuess = new JButton("Guess!");
        btnGuess.setBounds(172, 149, 89, 23);
        btnGuess.setFont(FONT_COMIK);
        btnGuess.addActionListener(e -> checkGuess());
        getContentPane().add(btnGuess);

        lblOutput = new JLabel("Enter a number above and click Guess!");
        lblOutput.setBounds(10, 209, 414, 14);
        lblOutput.setFont(FONT_COMIK);
        lblOutput.setHorizontalAlignment(JLabel.CENTER);
        getContentPane().add(lblOutput);

        setVisible(true);

    }

    private void stats() {
        JOptionPane.showMessageDialog(this, "<html> <h3> <i> Количество побед = " + properties.get("win") + "\n" +
                "<html> <h3> <i> Количество поражений = " + properties.get("lose") + "</i></h3>", "Game stats",
                JOptionPane.INFORMATION_MESSAGE);
    }

    private void writeProperties() {
        try (FileWriter fileWriter = new FileWriter(RESOURCES + "properties.txt")) {
            fileWriter.write("range=" + properties.get("range") + "\n" +
                    "win=" + properties.get("win") + "\n" +
                    "lose=" + properties.get("lose"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String getNewTitle() {
        return "Enter a number between 1 and " + properties.get("range") + ":";
    }

    private void aboutGame() {
        JOptionPane.showMessageDialog(this, "<html> <h2><i>(c) 2023 Igor.</i></h2>",
                "About Guessing Game", JOptionPane.INFORMATION_MESSAGE, iconAbout);
    }

    private void checkGuess() {
        String titleDialog;
        String messageDialog;
        int result = 3;
        try {
            int guess = Integer.parseInt(txtGuess.getText());
            numberOfTries--;
            boolean winer = false;

            if (guess < theNumber) {
                message = guess + " - is to low. Try again.";
            } else if (guess > theNumber) {
                message = guess + " - is to high. Try again.";
            } else {

                winer = true;

                Integer win = properties.get("win");
                properties.replace("win", (win + 1));

                messageDialog = "<html> <h3>" + guess + " - is correct.</h3> <h2><i>You win </i></h2> <h3>Let`s play again?</h3>";
                titleDialog = "WINNER";

                result = getDialog(messageDialog, titleDialog);


            }

            if (numberOfTries == 0 && !winer) {

                Integer lose = properties.get("lose");
                properties.replace("lose", (lose + 1));

                messageDialog = "<html> <h2><i>Game Over </i></h2> <h3>Let`s play again?</h3>";
                titleDialog = "LOSER";

                result = getDialog(messageDialog, titleDialog);
            }
        } catch (Exception e) {
            message = "Enter a number between 1 and " + properties.get("range") + "!";
        } finally {

            if (result == JOptionPane.YES_OPTION) {
                newGame();
            } else if (result == JOptionPane.NO_OPTION) {
                writeProperties();
                System.exit(0);
            }

            lblOutput.setText(message);
            txtGuess.selectAll();
            txtGuess.requestFocus();
        }
    }

    private int getDialog(String messageDialog, String titleDialog) {
        Icon currentIcon;

        if (titleDialog.equals("WINNER")) {
            currentIcon = iconWinner;
        } else {
            currentIcon = iconExit;
        }
        return JOptionPane.showConfirmDialog(this, messageDialog, titleDialog, JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, currentIcon);
    }

    public static void main(String[] args) {
        GuessingGame theGame = new GuessingGame();
        theGame.readProperties();
        theGame.newGame();
    }

    private void readProperties() {
        try (FileReader fileReader = new FileReader(RESOURCES + "properties.txt");
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {

                String[] split = line.split("=");
                properties.put(split[0], Integer.parseInt(split[1]));
            }


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void newGame() {
        switch (properties.get("range")) {
            case 10 -> {
                itemRangeOne.setSelected(true);
                numberOfTries = 4;
            }
            case 100 -> {
                itemRangeTwo.setSelected(true);
                numberOfTries = 7;
            }
            case 1000 -> {
                itemRangeThree.setSelected(true);
                numberOfTries = 12;
            }
        }

        theNumber = (int) (Math.random() * properties.get("range") + 1);

        txtGuess.setText("");

        lblGuess.setText(getNewTitle());

        message = "Enter a number above and click Guess!";
        lblOutput.setText(message);
    }
    }



